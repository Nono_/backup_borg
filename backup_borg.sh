#!/bin/bash

# General parameters
mount="/mnt/path/to/be/mounted"

#Borg parameters
repo="/mnt/path/to/be/mounted/depotname"
pass="passwordhere"
backup_dir="/etc /root /usr/local/bin /home"

# remote server parameters
remote_user="backupuser"
#remote_ip=`cat /home/ip_remote_server`
remote_ip="1.1.1.1"
remote_port="22"
remote_dir="/remote/backup/path/"
remote_mount="${remote_user}@${remote_ip}:${remote_dir} ${mount} -p ${remote_port}"

# Setting this, so the repo does not need to be given on the commandline:
export BORG_REPO=${repo}

# Setting this, so you won't be asked for your repository passphrase:
export BORG_PASSPHRASE=${pass}
# or this to ask an external program to supply the passphrase:
export BORG_PASSCOMMAND='pass show backup'

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

if mount | grep ${mount} > /dev/null;
then
        info "${mount} already mounted";
else
        info "Mount: sshfs ${remote_mount}"
        sshfs ${remote_mount}
        retour1=$?
        if [ $retour1 -eq 0 ];
        then
                info "Correctly mounted";
        else
                info "Not mounted, stop the operation";
                exit;
        fi
fi
while true;
do
        case "$1" in
                "mount" ) info "Just mounted, stop the operation"; umount ${mount}; exit;;
                "list" ) info "Listing"; borg list ::; umount ${mount}; exit;;
                * ) break;;
        esac
done

info "Starting backup: ${backup_dir}"

# Backup the most important directories into an archive named after
# the machine this script is currently running on:

borg create                     \
        --verbose                       \
        --filter AME                    \
        --list                          \
        --stats                         \
        --show-rc                       \
        --compression lz4               \
        --exclude-caches                \
        --exclude '/home/*/.cache/*'    \
        --exclude '/var/cache/*'        \
        --exclude '/var/tmp/*'          \
                                \
        ::'{hostname}-{now}'            \
        $backup_dir                             \

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

borg prune                          \
        --list                          \
        --prefix '{hostname}-'          \
        --show-rc                       \
        --keep-daily    7               \
        --keep-weekly   4               \
        --keep-monthly  6               \

                                prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

info "Unmounting ${mount}"

umount ${mount}

if [ ${global_exit} -eq 1 ];
then
        info "Backup and/or Prune finished with a warning"
fi

if [ ${global_exit} -gt 1 ];
then
        info "Backup and/or Prune finished with an error"
fi
exit ${global_exit}
